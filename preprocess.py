#!/usr/bin/env python
import sys

# Come up with interface for passing this array
substitutionDict = {
    "AmmoniumIngested"  : 0,
    "Ammonium"          : 1,
    "SizeOld"           : 2,
    "SizeNew"           : 3,
    "Carbon"            : 4,
    "Chlorophyll"       : 5,
    "NitrateIngested"   : 6,
    "Nitrate"           : 7,
    "SilicateIngested"  : 8,
    "Silicate"          : 9,
    "ZOld"              : 10,
    "ZNew"              : 11,
    "Stage"             : 12,
    "DissolvedAmmonium" : 0,
    "DissolvedNitrate"  : 1,
    "DissolvedSilicate" : 2,
    "Temperature"       : 3,
    "Irradiance"        : 4
}


def preprocess(filename, substitutions, output=sys.stdout):
    with open(filename, "r") as pyfile:
        source = pyfile.read()

    for old, new in substitutions.iteritems():
        source = source.replace("'{0}'".format(old), str(new))

    output.write(source)

def main(args):
    for file in args:
        preprocess(file, substitutionDict)

2
if __name__ == "__main__":
    main(sys.argv[1:])
