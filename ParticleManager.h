#ifndef PM_HPP
#define PM_HPP

void PM_stripInsignificant(int stage);
void PM_splitMLD(int target, int stage);
void PM_mergeMLD(int target, int stage);
void PM_splitLayers(int target, int stage, int top, int bottom);
void PM_mergeLayers(int target, int stage, int top, int bottom);

#endif
