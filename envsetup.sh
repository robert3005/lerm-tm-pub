#!/bin/sh
export OMP_NUM_THREADS=4
export OMP_PROC_BIND=TRUE
export OMP_SCHEDULE="dynamic,800"
export GCC_INC=$HOME/gcc/include/c++/4.9.0
export GCC_LIB=$HOME/gcc/lib64
export LD_LIBRARY_PATH=$PWD:$GCC_LIB:$LD_LIBRARY_PATH
