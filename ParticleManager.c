#include <stdlib.h>
#include "LERM.h"
#include <assert.h>

static int compare_int (const void *a, const void *b) {
  return (*(int*)a - *(int*)b );
}

static void insertIntoMaximum(int * indexes, float * sizes, int maxIndexes,
                              int length, int index, float size) {
  int i, j;
  i=0;
  if (length>0) {
    while (i<length && sizes[i]>size) i++;
    for (j=MIN(length, maxIndexes-1); j>i; j--) {
      sizes[j]=sizes[j-1];
      indexes[j]=indexes[j-1];
    }
  }
  if (i<maxIndexes) {
    sizes[i]=size;
    indexes[i]=index;
  }
}

static int * getTheXBiggest(int x, int stage, int top, int bottom) {
  int i;
  int * indexes;
  float * sizes;

  indexes = (int *) calloc (x, sizeof(int));
  sizes = (float *) calloc (x, sizeof(float));
  int noSoFar=0;
  for (i=0; i<agentCount[stage]; i++) {
    if ((agents[stage][i][Z_NEW]>=top) && (agents[stage][i][Z_NEW]<(bottom+1))) {
      insertIntoMaximum(indexes,sizes,x,noSoFar,i,agents[stage][i][C_NEW]);
      noSoFar++;
      if (noSoFar>x) noSoFar--;
    }
  }
  free(sizes);
  return indexes;
}

static void insertIntoMinimum(int * indexes, float * sizes, int maxIndexes,
                              int length, int index, float size) {
  int i, j;
  i=0;
  if (length>0) {
    while ((i<length) && (sizes[i]<size)) i++;
    for (j=MIN(length,maxIndexes-1); j>i; j--) {
      sizes[j]=sizes[j-1];
      indexes[j]=indexes[j-1];
    }
  }
  if (i<maxIndexes) {
    sizes[i]=size;
    indexes[i]=index;
  }
}

static int * getTheXSmallest(int x, int stage, int top, int bottom) {
  int i;
  int * indexes;
  float * sizes;

  indexes = (int *) calloc (x, sizeof(int));
  sizes = (float *) calloc (x, sizeof(float));
  int noSoFar=0;
  for (i=0; i<agentCount[stage]; i++) {
    if ((agents[stage][i][Z_NEW]>=top) && (agents[stage][i][Z_NEW]<(bottom+1))) {
      insertIntoMinimum(indexes,sizes,x,noSoFar,i,agents[stage][i][C_NEW]);
      noSoFar++;
      if (noSoFar>x) noSoFar--;
    }
  }
  free(sizes);
  return indexes;
}

static float wtavg(float v1, float w1, float v2, float w2) {
  return ((v1*w1)+(v2*w2))/(w1+w2);
}

static void mergeInto(float * p1, float * p2) {
  p1[AMMONIUM_POOL]=wtavg(p1[AMMONIUM_POOL],p1[C_NEW],p2[AMMONIUM_POOL],p2[C_NEW]);
  p1[CARBON_POOL]=wtavg(p1[CARBON_POOL],p1[C_NEW],p2[CARBON_POOL],p2[C_NEW]);
  p1[CHLOROPHYLL_POOL]=wtavg(p1[CHLOROPHYLL_POOL],p1[C_NEW],p2[CHLOROPHYLL_POOL],p2[C_NEW]);
  p1[NITRATE_POOL]=wtavg(p1[NITRATE_POOL],p1[C_NEW],p2[NITRATE_POOL],p2[C_NEW]);
  p1[SILICATE_POOL]=wtavg(p1[SILICATE_POOL],p1[C_NEW],p2[SILICATE_POOL],p2[C_NEW]);
  p1[AMMONIUM_ING]=wtavg(p1[AMMONIUM_ING],p1[C_NEW],p2[AMMONIUM_ING],p2[C_NEW]);
  p1[NITRATE_ING]=wtavg(p1[NITRATE_ING],p1[C_NEW],p2[NITRATE_ING],p2[C_NEW]);
  p1[SILICATE_ING]=wtavg(p1[SILICATE_ING],p1[C_NEW],p2[SILICATE_ING],p2[C_NEW]);

  /* In some merges, "parent" agent may change depth, so first remove from old stats...*/
  physics->agentsInLayer[(int)p1[STAGE]][(int)p1[Z_NEW]]--;
  if ((int)p1[Z_NEW]<(int)physics->turbocline) physics->agentsInMLD[(int)p1[STAGE]]--;

  /* Then update depth...*/
  p1[Z_OLD]=wtavg(p1[Z_OLD],p1[C_NEW],p2[Z_OLD],p2[C_NEW]);
  p1[Z_NEW]=wtavg(p1[Z_NEW],p1[C_NEW],p2[Z_NEW],p2[C_NEW]);

  /* Then add to stats at new depth.*/
  physics->agentsInLayer[(int)p1[STAGE]][(int)p1[Z_NEW]]++;
  if ((int)p1[Z_NEW]<(int)physics->turbocline) physics->agentsInMLD[(int)p1[STAGE]]++;

  p1[C_OLD]+=p2[C_OLD];
  p1[C_NEW]+=p2[C_NEW];
}

static void split(int i, int stage) {
  agents[stage][agentCount[stage]][C_OLD]=agents[stage][i][C_OLD]/2.0;
  agents[stage][agentCount[stage]][C_NEW]=agents[stage][i][C_NEW]/2.0;
  agents[stage][agentCount[stage]][AMMONIUM_ING]=agents[stage][i][AMMONIUM_ING];
  agents[stage][agentCount[stage]][AMMONIUM_POOL]=agents[stage][i][AMMONIUM_POOL];
  agents[stage][agentCount[stage]][CARBON_POOL]=agents[stage][i][CARBON_POOL];
  agents[stage][agentCount[stage]][CHLOROPHYLL_POOL]=agents[stage][i][CHLOROPHYLL_POOL];
  agents[stage][agentCount[stage]][NITRATE_ING]=agents[stage][i][NITRATE_ING];
  agents[stage][agentCount[stage]][NITRATE_POOL]=agents[stage][i][NITRATE_POOL];
  agents[stage][agentCount[stage]][SILICATE_ING]=agents[stage][i][SILICATE_ING];
  agents[stage][agentCount[stage]][SILICATE_POOL]=agents[stage][i][SILICATE_POOL];
  agents[stage][agentCount[stage]][Z_OLD]=agents[stage][i][Z_OLD];
  agents[stage][agentCount[stage]][Z_NEW]=agents[stage][i][Z_NEW];
  agents[stage][agentCount[stage]][STAGE]=agents[stage][i][STAGE];
  agents[stage][i][C_OLD]=agents[stage][i][C_OLD]/2.0;
  agents[stage][i][C_NEW]=agents[stage][i][C_NEW]/2.0;
  int theStage = (int) agents[stage][i][STAGE];
  float theZ = agents[stage][i][Z_NEW];

  physics->agentsInLayer[theStage][(int)theZ]++;
  if ((int)theZ<(int)physics->turbocline) physics->agentsInMLD[theStage]++;
  agentCount[stage]++;
  assert (agentCount[stage] <= MAX_AGENTS);
}

static void removeAgent(int index, int stage) {
  int i;
  /*
    printf ("removeAgent %d\n", index);
  */
  assert(agents[stage][index][STAGE] == stage);
  for (i=0; i<13; i++){
    agents[stage][index][i]=agents[stage][agentCount[stage]-1][i];
  }
  agentCount[stage]--;
}

void PM_splitMLD(int target, int stage) {
  int i;
  target*=(int) physics->turbocline;
  while (target>physics->agentsInMLD[stage]) {
    int splitsWanted = MIN(target-physics->agentsInMLD[stage],physics->agentsInMLD[stage]);

    int * indexes = getTheXBiggest(splitsWanted,stage,0,(int)physics->turbocline);
    for (i=0; i<splitsWanted; i++) {
      split(indexes[i], stage);
    }
    free(indexes);
  }
}

void PM_mergeMLD(int target, int stage) {
  int i;
  target*=(int) physics->turbocline;

  while (target<physics->agentsInMLD[stage]) {
    int mergesWanted = MIN(physics->agentsInMLD[stage]-target,physics->agentsInMLD[stage]/2);
    int *indexes = getTheXSmallest(mergesWanted*2,stage,0,(int)physics->turbocline);

    /* Sorts indexes in ascending order, then merge/remove from the end first.*/
    qsort(indexes, 2*mergesWanted, sizeof(int), compare_int);

    for (i=2*mergesWanted-2; i>=0; i-=2)
      mergeInto(agents[stage][indexes[i]], agents[stage][indexes[i+1]]);
    for (i=2*mergesWanted-1; i>=0; i-=2)
      removeAgent(indexes[i],stage);
    physics->agentsInMLD[stage]-=mergesWanted;
    free(indexes);
  }
}

void PM_mergeLayers(int target, int stage, int top, int bottom) {
  int i, layer;
  int mergeTotal = 0;

  for (layer=top; layer<=bottom; layer++) {
    while (physics->agentsInLayer[stage][layer]>target) {
      int agLayerB4 = physics->agentsInLayer[stage][layer];
      int mergesWanted = MIN(physics->agentsInLayer[stage][layer]-target,physics->agentsInLayer[stage][layer]/2);
      int *indexes = getTheXSmallest(mergesWanted*2,stage,layer,layer);

      /* Sorts indexes in ascending order, then merge/remove from the end first.*/
      qsort(indexes, 2*mergesWanted, sizeof(int), compare_int);

      for (i=2*mergesWanted-2; i>=0; i-=2)
	mergeInto(agents[stage][indexes[i]],agents[stage][indexes[i+1]]);
      for (i=2*mergesWanted-1; i>=0; i-=2)
	removeAgent(indexes[i],stage);
      physics->agentsInLayer[stage][layer]-=mergesWanted;

      assert(physics->agentsInLayer[stage][layer] == agLayerB4 - mergesWanted);
      mergeTotal += mergesWanted;
      free(indexes);
    }
  }
  deadMerge += mergeTotal;
}

/* splitLayers is not used in the "default" model, but is here so we can test more p.m.*/
void PM_splitLayers(int target, int stage, int top, int bottom) {
  int i, layer;
  for (layer=top; layer<=bottom; layer++) {
    while (physics->agentsInLayer[stage][layer]<target) {
      int agLayerB4 = physics->agentsInLayer[stage][layer];
      int splitsWanted = MIN(target-physics->agentsInLayer[stage][layer],physics->agentsInLayer[stage][layer]);
      int * indexes = getTheXBiggest(splitsWanted,stage,0,(int)physics->turbocline);
      for (i=0; i<splitsWanted; i++){
	split(indexes[i], stage);
      }
      free(indexes);
      assert(physics->agentsInLayer[stage][layer] == agLayerB4 - splitsWanted);
    }
  }
}


/* This one is actually important - if you merge c=1E-200 with c=1E-10, very   *
 * bad things happen, and all the maths goes enormously wrong. So remove small *
 * agents, and also those that have fallen out of the bottom.                  */
void PM_stripInsignificant(int stage) {
  int i;
  for(stage=0; stage<2; stage++){
    for (i=agentCount[stage]-1; i>=0; i--) {
      if ((agents[stage][i][C_NEW]<1E-10) || (agents[stage][i][Z_NEW]>=500))
	removeAgent(i,stage);
    }
  }
}

/* This biomass checker is just for debugging. Biomass before any *
 * pm should be == biomass after. (+/- rounding)
static float getBiomass(int stage, int top, int bottom) {
  int i;
  float biomass=0;
  for (i=0; i<noAgents; i++) {
    if (((int)agents[i][STAGE]==stage) &&
	(agents[i][Z_NEW]>=top) &&
	(agents[i][Z_NEW]<bottom+1)) {
      biomass+=agents[i][C_NEW];
    }
  }
  return biomass;
}*/
