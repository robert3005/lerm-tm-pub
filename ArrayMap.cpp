#include "ArrayMap.h"

template<typename K, typename V>
V& ArrayMap<K, V>::operator[](const K& key) {
    return this->dataArray[this->dataMap[key]];
};

template<typename K, typename V>
const V& ArrayMap<K, V>::operator[](const K& key) const {
    return const_cast<V&>((*this)[key]);
};

template<typename K, typename V>
void ArrayMap<K, V>::setData(V * dataArray) {
    this->dataArray = dataArray;
};

template<typename K, typename V>
void ArrayMap<K, V>::setMapper(std::unordered_map<K, size_t>& mapper) {
    this->dataMap = mapper;
}

