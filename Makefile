EXECUTABLE = LERM

OBJS       = ParticleManager.o LERM.o

CC         = $(HOME)/gcc/bin/gcc
CXX        = $(HOME)/gcc/bin/g++
CFLAGS     = -O2 -Wall -fdump-tree-optimized -std=gnu99 $(shell python-config --includes) -I$(shell pwd) -fopenmp -lrt -I$(GCC_INC)

ifdef WITH_OPENMP
CFLAGS += -DWITH_OPENMP -DPHYS_CORES=$(OMP_NUM_THREADS)
endif

LINKER     = $(HOME)/gcc/bin/gcc
LDFLAGS    = -lstdc++ -L$(shell pwd) -lgomp $(shell python-config --ldflags) -ldiatom -lrt -L$(GCC_LIB)
.PHONY: all clean cython annotate results

all: cython $(EXECUTABLE) results

results:
	mkdir -p ../results

cython:
	CC=$(CC) CXX=$(CXX) python build.py

annotate: cython
	xdg-open lerm_diatom/lerm_diatom.html
	xdg-open libdiatom.html

annotate:
	cython --cplus --annotate lerm_diatom/lerm_diatom.pyx libdiatom.pyx
	xdg-open lerm_diatom/lerm_diatom.html
	xdg-open libdiatom.html

.c.o:
	$(CC) $(CFLAGS) -c $<

$(EXECUTABLE) : $(OBJS)
	$(LINKER) -o $(EXECUTABLE) $(OBJS) $(LDFLAGS)

clean:
	git clean -xfd
