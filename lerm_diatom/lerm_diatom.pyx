from libc.math cimport exp, pow

# Parameters for FGroup Diatom
# Species: Default_Diatom_Variety
DEF param_A_E           = -10000.0
DEF param_Alpha_Chl     = 7.9e-07
DEF param_C_minS        = 1.584e-08
DEF param_C_rep         = 1.76e-08
DEF param_C_starve      = 8.5e-09
DEF param_C_struct      = 8.5e-09
DEF param_Ndis          = 0.0042
DEF param_P_ref_c       = 0.14
DEF param_Q_Nmax        = 0.17
DEF param_Q_Nmin        = 0.034
DEF param_Q_S_max       = 0.15
DEF param_Q_S_min       = 0.04
DEF param_Q_remN        = 2.95
DEF param_Q_remS        = 2.27
DEF param_R_Chl         = 0.002
DEF param_R_N           = 0.002
DEF param_R_maintenance = 0.002
DEF param_S_dis         = 0.00083
DEF param_S_rep         = 2.1e-09
DEF param_T_ref         = 293.0
DEF param_T_refN        = 283.0
DEF param_T_refS        = 278.0
DEF param_Theta_max_N   = 4.2
DEF param_V_S_ref       = 0.03
DEF param_V_ref_c       = 0.01
DEF param_Zeta          = 2.3
DEF param_k_AR          = 1.0
DEF param_k_S           = 1.0
DEF param_z_sink        = 0.04


# Timestep in seconds
cdef double dt = 1800.0

cdef void update_Living_Diatom(float * vars, float * env, float * rel) nogil:
    """ FGroup:  Diatom
        Stage:   Living
    """
    cdef double dt_in_hours = dt / 3600.0

    cdef double size = vars[2]
    ### Effect of temperature ###
    cdef double T_K = (env[0] + 273.0)
    cdef double T_function = exp((param_A_E * ((1.0 / T_K) - (1.0 / param_T_ref))))

    ### Photosynthesis ###
    cdef double Q_N = ((vars[1] + vars[0] + vars[7] + vars[6]) / vars[4])

    cdef double Q_s = ((vars[9] + vars[8]) / vars[4])
    cdef double P_max_c = (((param_P_ref_c * T_function)) if (Q_N > param_Q_Nmax) else (((0.0) if (Q_N < param_Q_Nmin) else ((param_P_ref_c * T_function * ((Q_N - param_Q_Nmin) / (param_Q_Nmax - param_Q_Nmin)))))))
    cdef double Theta_c = (vars[5] / vars[4])
    cdef double E_0 = (4.6 * env[1])
    cdef double P_phot_c = ((0.0) if ((P_max_c == 0.0) or (Q_s <= param_Q_S_min)) else ((P_max_c * (1.0 - exp(((-3600.0 * param_Alpha_Chl * Theta_c * E_0) / P_max_c))))))

    ### Chlorophyll Synthesis ###
    cdef double Theta_N = (vars[5] / (vars[1] + vars[0] + vars[7] + vars[6]))
    cdef double Rho_Chl = (((param_Theta_max_N * (P_phot_c / (3600.0 * param_Alpha_Chl * Theta_c * E_0)))) if ((E_0 > 0.0) and (Theta_c > 0.0)) else (0.0))

    ### Respiration ###
    cdef double R_C_growth = (((vars[0] + vars[6]) * param_Zeta) / (dt_in_hours * vars[4]))
    cdef double R_C = (param_R_maintenance + R_C_growth)

    ### Cell Division ###
    cdef double C_d = ((2.0) if (((vars[4] + (vars[4] * (P_phot_c - R_C) * dt_in_hours)) >= param_C_rep) and ((vars[9] + vars[8]) >= param_S_rep)) else (1.0))
    if (C_d == 2.0):
        vars[3] = vars[2] * 2.0

    ### Nutrients uptake ###
    cdef double Q_nitrate = ((vars[7] + vars[6]) / vars[4])
    cdef double Q_ammonium = ((vars[1] + vars[0]) / vars[4])
    cdef double omega = ((param_k_AR / (param_k_AR + env[2])) * ((param_k_AR + env[3]) / (param_k_AR + env[2] + env[3])))
    cdef double V_max_C = (((((param_V_ref_c * T_function)) if ((Q_ammonium + Q_nitrate) < param_Q_Nmin) else (((0.0) if ((Q_ammonium + Q_nitrate) > param_Q_Nmax) else ((param_V_ref_c * pow(((param_Q_Nmax - (Q_ammonium + Q_nitrate)) / (param_Q_Nmax - param_Q_Nmin)), 0.05) * T_function)))))) if ((vars[1] + vars[7]) < 1000.0) else (0.0))
    cdef double V_C_ammonium = (V_max_C * (env[2] / (param_k_AR + env[2])))
    cdef double V_C_nitrate = (V_max_C * (env[3] / (param_k_AR + env[3])) * omega)
    cdef double V_S_max = (((((param_V_S_ref * T_function)) if (Q_s <= param_Q_S_min) else (((0.0) if (Q_s >= param_Q_S_max) else ((param_V_S_ref * pow(((param_Q_S_max - Q_s) / (param_Q_S_max - param_Q_S_min)), 0.05) * T_function)))))) if (vars[4] >= param_C_minS) else (0.0))
    cdef double V_S_S = (V_S_max * (env[4] / (env[4] + param_k_S)))
    cdef double ammUptake = size * (vars[4] * V_C_ammonium * dt_in_hours)
    cdef double nitUptake = size * (vars[4] * V_C_nitrate * dt_in_hours)
    cdef double silUptake = size * (vars[9] * V_S_S * dt_in_hours)

    ### Update Pools ###
    cdef double Ammonium_new = ((((vars[1] + vars[0] + vars[6]) - (vars[1] * param_R_N * dt_in_hours * T_function))) / C_d)
    cdef double Nitrate_new = 0.0
    cdef double Silicate_new = (((vars[9] + vars[8]) - 0.0) / C_d)
    cdef double death_flag = ((1.0) if (vars[4] <= param_C_starve) else (0.0))
    if (1.0) == death_flag:
        vars[12] = 1.0

    cdef double Carbon_new = max(0.0, (((vars[4] * (P_phot_c - (R_C * T_function)) * dt_in_hours) + vars[4]) / C_d)) if (death_flag == 0.0) else (0.0)
    cdef double Chlorophyll_new = ((max((((((vars[5] + (Rho_Chl * (vars[0] + vars[6])))) if (Theta_N <= param_Theta_max_N) else ((vars[5] - (vars[5] - ((vars[1] + vars[7]) * param_Theta_max_N))))) - ((vars[5] * param_R_Chl * dt_in_hours * T_function) + 0.0)) / C_d), 0.0)) if (death_flag == 0.0) else (0.0))

    ### Remineralisation Nitrogen ###
    rel[0] = size * ((vars[1] + vars[7]) * param_R_N * dt_in_hours * T_function)

    ### Setting pool variables
    vars[0] = ammUptake
    vars[6] = nitUptake
    vars[8] = silUptake
    vars[1] = Ammonium_new
    vars[7] = Nitrate_new
    vars[9] = Silicate_new
    vars[4] = Carbon_new
    vars[5] = Chlorophyll_new


cdef void update_Dead_Diatom(float * vars, float * env, float * rel) nogil:
    """ FGroup:  Diatom
        Stage:   Dead
    """
    cdef double dt_in_hours = dt / 3600.0

    ### Remineralisation Dead T ###
    cdef double Si_reminT = (param_S_dis * pow(param_Q_remS, (((env[0] + 273.0) - param_T_refS) / 10.0)))
    cdef double N_reminT = (param_Ndis * pow(param_Q_remN, (((env[0] + 273.0) - param_T_refN) / 10.0)))
    rel[1] = vars[2] * (vars[9] * Si_reminT * dt_in_hours)
    cdef double Silicate_new = max(((vars[9] + vars[8]) - (vars[9] * Si_reminT * dt_in_hours)), 0.0)
    rel[0] = vars[2] * ((vars[1] + vars[7]) * N_reminT * dt_in_hours)
    cdef double Ammonium_new = max(((vars[1] + vars[0]) - (vars[1] * N_reminT * dt_in_hours)), 0.0)
    cdef double Nitrate_new = max(((vars[7] + vars[6]) - (vars[7] * N_reminT * dt_in_hours)), 0.0)

    ### Setting pool variables
    vars[9] = Silicate_new
    vars[1] = Ammonium_new
    vars[7] = Nitrate_new
