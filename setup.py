import os
from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext, Extension
from Cython.Compiler import Options

# generate annotations
Options.annotate = True
# Don't raise python exceptions on division by 0
# Necessary for gil free execution
# Per module settings are ignored - set globally
Options.directive_defaults["cdivision"] = True
HOME = os.environ['HOME']

extensions = [
    Extension("libdiatom", ["libdiatom.pyx"]),
    Extension("lerm_diatom.lerm_diatom", ["lerm_diatom/lerm_diatom.pyx"])
]

for e in extensions:
    e.include_dirs = ['.']
    e.language = "c++"
    e.extra_compile_args = ["-fopenmp", "-I{0}/gcc/include/c++/4.9.0".format(HOME)]
    e.extra_link_args = ["-fopenmp", "-L{0}/gcc/lib64".format(HOME), "-lstdc++"]

setup(
    name = "LERM",
    cmdclass = {"build_ext": build_ext},
    packages = ["lerm_diatom"],
    ext_modules = cythonize(extensions)
)
