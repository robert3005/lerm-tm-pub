#ifndef LERM_HPP
#define LERM_HPP

#include <stdio.h>
#include <sys/timeb.h>

/* Chemistry Indexes */
#define AMMONIUM             0
#define NITRATE              1
#define SILICATE             2

#define AMMONIUM_ING         0          /* Ammonium Uptake */
#define AMMONIUM_POOL        1          /* Current ammonium pool */
#define C_OLD                2          /* Sub-population size last timestep */
#define C_NEW                3          /* Sub-population size next timestep */
#define CARBON_POOL          4          /* Carbon pool */
#define CHLOROPHYLL_POOL     5          /* Chlorophyll pool */
#define NITRATE_ING          6          /* nitrate uptake */
#define NITRATE_POOL         7          /* Nitrate pool */
#define SILICATE_ING         8          /* Silicate uptake */
#define SILICATE_POOL        9          /* Silicate pool */
#define Z_OLD                10         /* Depth last timestep */
#define Z_NEW                11         /* Depth next timestep */
#define STAGE                12         /* Stage (Living=0, Dead=1) */
#define AMMONIUM_REL		 13			/* Ammonium released */
#define SILICATE_REL		 14			/* Silicate released */

#define _STAGE_Living        0
#define _STAGE_Dead          1

/*
 * Defines static max length of agent arrays
 * Since its static we just over-allocate
 */
#define MAX_AGENTS           640000

/* Initial no  agents */
int AGENT_LIVING_INIT;
int AGENT_DEAD_INIT;

/* Macros */
#define MAX(A,B)             (((A)>=(B))?(A):(B))
#define MIN(A,B)             (((A)<(B))?(A):(B))

/* Constant data:
 * This is computed by the environment
 * and distributed to all agents
 * (values are constant throughout time step)
 */
struct constUpdate {
	float chem_conc[500][3];        /* Concentration */
	float chem_depletion[500][3];   /* Depletion factor (0..1) */
	float phyto_dead_depletion[500];  /* Biomass of dead Diatoms to be added to dead agents each time step*/
	float phyto_dead_chem[500][3];  /* Chemical pools of dead Diatoms */
};

/* Physics Environment */
struct physics_data {
	float depth[765];               /* Absolute depth of physics layer[i]. */
	float temperature[765];         /* Temperature[i] at depth[i] */
	float vis_irradiance[765];      /* Visible irradiance[i] at depth[i] */
	int noPhysicsLayers;             /* No. of layers (dynamic) */
	float turbocline;

	int agentsInLayer[2][500];       /* [stage][layer] */
	int agentsInMLD[2];              /* [stage] */
};

/* Global Variables */
struct constUpdate *constData;
struct physics_data *physics;

/* Chemistry Environment */
float chem_req[500][3];         /* Total requested for uptake */
float chem_remin[500][3];       /* Total remineralised */

/* Agents and counters */
int agentCount[2];
float agents[2][MAX_AGENTS][13];	/* Agents, and variables for each */

/* Other Housekeeping */
float biomass[2];
float stepInHours;
int timestep;

int deadMerge;

FILE *physicsFile;
FILE *chemistryFile;
FILE *outputFile;

#endif
