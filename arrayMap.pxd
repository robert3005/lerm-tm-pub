from libcpp.unordered_map cimport unordered_map

cdef extern from "ArrayMap.h":
    cdef cppclass ArrayMap[K, V]:
        ArrayMap() nogil except +
        ArrayMap(unordered_map[K, size_t] mapper) nogil except +
        ArrayMap(V * data, unordered_map[K, size_t] mapper) nogil except +

        void setData(V *) nogil
        void setMapper(unordered_map[K, size_t] &) nogil
        V& operator[](K&) nogil
