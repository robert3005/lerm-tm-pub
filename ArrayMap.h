#ifndef __HAS_ARRAYMAP
#define __HAS_ARRAYMAP

#include <unordered_map>
#include <cstring>

template<typename K, typename V>
class ArrayMap {
    private:
        V * dataArray;
        std::unordered_map<K, size_t> dataMap;

    public:
        ArrayMap() {};
        ArrayMap(std::unordered_map<K, size_t>& mapper) {
            this->setMapper(mapper);
        };
        ArrayMap(V * data, std::unordered_map<K, size_t>& mapper) : dataArray(data) {
            this->setMapper(mapper);
        };
        ~ArrayMap() {};
        void setData(V *);
        void setMapper(std::unordered_map<K, size_t>&);
        V& operator[](const K&);
        const V& operator[](const K&) const;
};

#include "ArrayMap.cpp"

#endif
