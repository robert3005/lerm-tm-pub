cimport lerm_diatom.lerm_diatom as diatom

cdef public void _updateLivingDiatom(float * vars, float temp, float vis_irradiance, float * chem, float * rel) nogil:
    cdef float * chemVals = [temp, vis_irradiance, chem[0], chem[1], chem[2]]
    diatom.update_Living_Diatom(vars, chemVals, rel)


cdef public void _updateDeadDiatom(float * vars, float temp, float * rel) nogil:
    cdef float * chemVals = [temp]
    diatom.update_Dead_Diatom(vars, chemVals, rel)
