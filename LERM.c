#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "LERM.h"
#include "ParticleManager.h"
#include <Python.h>
#include "libdiatom.h"
#include <time.h>
#include <sys/time.h>
#include <omp.h>

// Fix Ctrl-C
static void handler(int sig) {
  exit(sig);
}

/* Model Constants */
#define param_z_sink         0.04

clock_t t1,t2;
float ratio;
float totalTime;

/* Per thread values of chemistry environment. */
#ifdef WITH_OPENMP
  float chem_req_thread[PHYS_CORES][500][3];
  float chem_remin_thread[PHYS_CORES][500][3];
  float biomass_thread[PHYS_CORES][200];
  int agentsInLayer_thread[PHYS_CORES][2][500];
  int agentsInMLD_thread[PHYS_CORES][2];
#endif

/* Local Routines */

static float rnd(float a) {
  /* This is the seed for the random number generator */
  static int i = 79654659;
  #ifdef WITH_OPENMP
    #pragma omp threadprivate(i)
  #endif
  float n;

  i = (i * 125) % 2796203;
  n = (i % (int) a) + 1.0;
  return n;
}

static int revIntByteOrder(int i) {
  char *c1, *c2;
  int ri;

  c1 = (char *)&i;
  c2 = (char *)&ri;

  c2[0] = c1[3];
  c2[1] = c1[2];
  c2[2] = c1[1];
  c2[3] = c1[0];

  return (ri);
}

static double revDoubleByteOrder(double d) {
  char *c1, *c2;
  double rd;

  c1 = (char *)&d;
  c2 = (char *)&rd;

  c2[0] = c1[7];
  c2[1] = c1[6];
  c2[2] = c1[5];
  c2[3] = c1[4];
  c2[4] = c1[3];
  c2[5] = c1[2];
  c2[6] = c1[1];
  c2[7] = c1[0];

  return (rd);
}

static int getIntFromFile(FILE * fp) {
  int value;
  assert (fread (&value, sizeof(int), 1, fp) != sizeof(int));
  return (revIntByteOrder(value));
}

static double getDoubleFromFile(FILE * fp) {
  double value;
  assert (fread (&value, sizeof(double), 1, fp) != sizeof(double));
  return (revDoubleByteOrder(value));
}

static void readPhysics() {
  int i;

  physics->turbocline = (float)getDoubleFromFile(physicsFile);
  physics->noPhysicsLayers = getIntFromFile(physicsFile);

  for (i=0; i<physics->noPhysicsLayers; i++) {
    physics->depth[i] = (float)getDoubleFromFile(physicsFile);
    physics->temperature[i] = (float)getDoubleFromFile(physicsFile);
    physics->vis_irradiance[i] = (float)getDoubleFromFile(physicsFile);
  }
}

/* PLayer conversion */
static int getPLayer(float dd) {
  dd+=0.00001;

  int d = (int) floor(dd);
  if (dd >= 499) {
    return (physics->noPhysicsLayers - 1);
  } else if (dd == 0) {
    return 0;
  } else if (dd > 1) {
    int i = 21;
    if (physics->depth[d + i] < dd) {
      while (physics->depth[d + i] < dd) i++;
      return (d + i - 1);
    } else if (physics->depth[d + i] > dd) {
      while (physics->depth[d + i] > dd) i--;
      return d + i;
    } else {
      return d + i;
    }
  } else {
    int i = 0;
    while (physics->depth[i] < dd) i++;
    return (i - 1);
  }
}

/*****************************/
/* MAIN PLANKTON UPDATE CODE */
/*****************************/

static void updateLivingDiatom(float *vars) { //int agent) {

  /* Phase 1
   * (import and initial housekeeping)
   */
  vars[C_OLD] = vars[C_NEW];
  vars[Z_OLD] = vars[Z_NEW];
  vars[AMMONIUM_ING] /= vars[C_OLD];
  vars[NITRATE_ING]  /= vars[C_OLD];
  vars[SILICATE_ING] /= vars[C_OLD];

  /* External environmnet */
  int z_old_int = (int) (vars[Z_OLD]);
  int pLayer = getPLayer(vars[Z_NEW]);
  float _ambientTemperature = physics->temperature[pLayer];
  float _ambientVisIrrad = physics->vis_irradiance[pLayer];

  /* Nutrient uptake */
  if (constData->chem_depletion[z_old_int][AMMONIUM]<1)
    vars[AMMONIUM_ING] *= constData->chem_depletion[z_old_int][AMMONIUM];
  if (constData->chem_depletion[z_old_int][NITRATE]<1)
    vars[NITRATE_ING] *= constData->chem_depletion[z_old_int][NITRATE];
  if (constData->chem_depletion[z_old_int][SILICATE]<1)
    vars[SILICATE_ING] *= constData->chem_depletion[z_old_int][SILICATE];

  /* Phase 2
   * This is the arithmetic meat and where most of the workload lies
   * (this gets auto-generated)
   */
  float rel[1];
  _updateLivingDiatom(vars, _ambientTemperature, _ambientVisIrrad, constData->chem_conc[z_old_int], rel);

  /* Motion */
  float random  = rnd(physics->turbocline);
  vars[Z_NEW]   = (((vars[Z_OLD]) <= (physics->turbocline))?(((random)+(((param_z_sink)*(stepInHours))))):(((vars[Z_OLD])+(((param_z_sink)*(stepInHours))))));
  int z_new_int = (int)(vars[Z_NEW] + 0.0001f);

  #ifndef WITH_OPENMP
  //update external
  chem_req[z_new_int][SILICATE]   += vars[SILICATE_ING];
  chem_req[z_new_int][NITRATE]    += vars[NITRATE_ING];
  chem_req[z_new_int][AMMONIUM]   += vars[AMMONIUM_ING];
  chem_remin[z_new_int][AMMONIUM] += rel[0];
  /* housekeeping */
  biomass[(int)(vars[STAGE] + 0.0001f)] += vars[C_NEW];

  if (vars[Z_NEW] < 500) {
    physics->agentsInLayer[(int)vars[STAGE]][(int)vars[Z_NEW]]++;
  }

  if (vars[Z_NEW] < (int)physics->turbocline) {
    physics->agentsInMLD[(int)vars[STAGE]]++;
  }
  #else
  int tid = omp_get_thread_num();
  chem_req_thread[tid][z_new_int][SILICATE]   += vars[SILICATE_ING];
  chem_req_thread[tid][z_new_int][NITRATE]    += vars[NITRATE_ING];
  chem_req_thread[tid][z_new_int][AMMONIUM]   += vars[AMMONIUM_ING];
  chem_remin_thread[tid][z_new_int][AMMONIUM] += rel[0];
  /* housekeeping */
  biomass_thread[tid][(int)(vars[STAGE] + 0.0001f)] += vars[C_NEW];

  if (vars[Z_NEW] < 500) {
    agentsInLayer_thread[tid][(int)vars[STAGE]][(int)vars[Z_NEW]]++;
  }

  if (vars[Z_NEW] < (int)physics->turbocline) {
    agentsInMLD_thread[tid][(int)vars[STAGE]]++;
  }
  #endif
}

static void updateDeadDiatom(float *vars) {
  vars[C_OLD] = vars[C_NEW];
  vars[Z_OLD] = vars[Z_NEW];
  vars[AMMONIUM_ING] /= vars[C_OLD];
  vars[NITRATE_ING]  /= vars[C_OLD];
  vars[SILICATE_ING] /= vars[C_OLD];
  int z_old_int = (int) vars[Z_OLD];

  if (constData->chem_depletion[z_old_int][AMMONIUM]<1)
    vars[AMMONIUM_ING]*=constData->chem_depletion[z_old_int][AMMONIUM];
  if (constData->chem_depletion[z_old_int][NITRATE]<1)
    vars[NITRATE_ING]*=constData->chem_depletion[z_old_int][NITRATE];
  if (constData->chem_depletion[z_old_int][SILICATE]<1)
    vars[SILICATE_ING]*=constData->chem_depletion[z_old_int][SILICATE];

  float _ambientTemperature = physics->temperature[getPLayer(vars[Z_OLD])];
  float updatedVars[2];
  _updateDeadDiatom(vars, _ambientTemperature, updatedVars);

  vars[CARBON_POOL]      = 0;
  vars[CHLOROPHYLL_POOL] = 0;
  vars[AMMONIUM_ING]     = 0;
  vars[NITRATE_ING]      = 0;
  vars[SILICATE_ING]     = 0;
  vars[Z_NEW]=(((vars[Z_OLD]) <= (physics->turbocline))?(((rnd(physics->turbocline))+(((param_z_sink)*(stepInHours))))):(((vars[Z_OLD])+(((param_z_sink)*(stepInHours))))));

  #ifndef WITH_OPENMP
  chem_remin[z_old_int][SILICATE] += updatedVars[1];
  chem_remin[z_old_int][AMMONIUM] += updatedVars[0];

  if (vars[Z_NEW]<500) {
    physics->agentsInLayer[_STAGE_Dead][(int)vars[Z_NEW]]++;
  }

  if (vars[Z_NEW]<(int)physics->turbocline){
    physics->agentsInMLD[_STAGE_Dead]++;
  }

  biomass[_STAGE_Dead] += vars[C_NEW];
  #else
  int tid = omp_get_thread_num();
  chem_remin_thread[tid][z_old_int][SILICATE] += updatedVars[1];
  chem_remin_thread[tid][z_old_int][AMMONIUM] += updatedVars[0];

  if (vars[Z_NEW]<500) {
    agentsInLayer_thread[tid][_STAGE_Dead][(int)vars[Z_NEW]]++;
  }

  if (vars[Z_NEW]<(int)physics->turbocline){
    agentsInMLD_thread[tid][_STAGE_Dead]++;
  }

  biomass_thread[tid][_STAGE_Dead] += vars[C_NEW];

  #endif
}

/**********************************************
 *   Control functions
 **********************************************/

static void initialisePlankton() {
  float _depth;
  agentCount[_STAGE_Living] = 0;
  agentCount[_STAGE_Dead] = 0;


  /* Initialize living agents evenly spaced over top 200m */
  for (_depth=0; _depth<200; _depth+=(float)200 / AGENT_LIVING_INIT) {
    agents[_STAGE_Living][agentCount[_STAGE_Living]][Z_OLD] = _depth;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][Z_NEW] = _depth;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][C_OLD] = 50000 / (float)AGENT_LIVING_INIT * 4000.0f;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][C_NEW] = 50000 / (float)AGENT_LIVING_INIT * 4000.0f;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][STAGE] = _STAGE_Living;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][AMMONIUM_POOL] = 2.2E-9;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][CARBON_POOL] = 1.5E-8;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][CHLOROPHYLL_POOL] = 2.7E-9;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][NITRATE_POOL] = 0.0;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][SILICATE_POOL] = 1.05E-9;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][AMMONIUM_ING] = 0.0;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][NITRATE_ING] = 0.0;
    agents[_STAGE_Living][agentCount[_STAGE_Living]][SILICATE_ING] = 0.0;
    biomass[_STAGE_Living] += 50000 / (float)AGENT_LIVING_INIT * 4000.0f;;
    agentCount[_STAGE_Living]++;
  }

}

/********************/
/* Handle chemistry */
/********************/

static void initialiseChemistry() {
  int i;

  for (i = 0; i < 500; i++) {
    constData->chem_conc[i][AMMONIUM] = getDoubleFromFile(chemistryFile);
    constData->chem_conc[i][NITRATE] = getDoubleFromFile(chemistryFile);
    constData->chem_conc[i][SILICATE] = getDoubleFromFile(chemistryFile);

    constData->chem_depletion[i][AMMONIUM] = 1.;
    constData->chem_depletion[i][NITRATE] = 1.;
    constData->chem_depletion[i][SILICATE] = 1.;
  }
}

static void mixChemistry() {
  int i, j, layer;
  float chemicalTotals[3];

  int mldLayers = 0;
  for (i = 0; i < 3; i++) chemicalTotals[i] = 0.0;
  for (layer = 0; layer < 500; layer++) {
    if (layer < physics->turbocline) {
      for (i = 0; i < 3; i++)
    chemicalTotals[i] += constData->chem_conc[layer][i];
      mldLayers++;
    }
  }
  for (j = 0; j < 3; j++)
    chemicalTotals[j] /= mldLayers;
  for (layer = 0; layer < mldLayers; layer++)
    for (j = 0; j < 3; j++) {
      constData->chem_conc[layer][j] = chemicalTotals[j];
    }

}

static void updateChemistry() {
  int i, c;
  for (i = 0; i < 500; i++) {
    for (c = 0; c < 3; c++) {
      if (chem_req[i][c] > constData->chem_conc[i][c]) {
    constData->chem_depletion[i][c] = constData->chem_conc[i][c] / chem_req[i][c];
    constData->chem_conc[i][c] = 0.;
      } else {
    constData->chem_depletion[i][c] = 1.;
    constData->chem_conc[i][c] -= chem_req[i][c];
      }
      constData->chem_conc[i][c] += chem_remin[i][c];
      chem_remin[i][c] = 0;
      chem_req[i][c] = 0;
    }
  }
}

/********************************
 *   Particle Mangement rules
 ********************************/

static void particleManagement() {
  /* PM only needs to be run once a day for the biology to work
   * So this is a valid move if needed */
  //if (timestep % 48 == 0) {
  PM_mergeLayers(1, _STAGE_Dead, 0, 499);

  /* original setup: 40 - 20 (with 4000 living agents) */
  PM_mergeMLD(AGENT_LIVING_INIT / 100, _STAGE_Living);
  PM_splitMLD(AGENT_LIVING_INIT / 200, _STAGE_Living);
  //}

  /* This catches agents that
   *  - are too small (1.0E-10)
   *  - have fallen out of the bottom of the column
   */
  PM_stripInsignificant(_STAGE_Living);
  PM_stripInsignificant(_STAGE_Dead);

  for (int i = 0; i < 500; i++) {
    physics->agentsInLayer[_STAGE_Living][i] = 0;
    physics->agentsInLayer[_STAGE_Dead][i] = 0;
  }
  physics->agentsInMLD[_STAGE_Living] = 0;
  physics->agentsInMLD[_STAGE_Dead] = 0;
}

static void updateAgentArrays() {
  int agent = 0;
  while (agent < agentCount[_STAGE_Living]) {
    if (agents[_STAGE_Living][agent][STAGE] == _STAGE_Dead) {
      int var;
      /* This copy moves agents that have just died to the new array */
      for(var = 0; var < 13; var++) {
    agents[_STAGE_Dead][agentCount[_STAGE_Dead]][var] = agents[_STAGE_Living][agent][var];
    agents[_STAGE_Living][agent][var] = agents[_STAGE_Living][agentCount[_STAGE_Living]-1][var];
      }
      agentCount[_STAGE_Living]--;
      agentCount[_STAGE_Dead]++;
    }
    else {
      agent++;
    }
  }
}

#ifdef WITH_OPENMP
  static void clearThreadsEnv() {
    #ifdef WITH_OPENMP
    #pragma omp parallel
    {
    #endif
      int tid = 0;
    #ifdef WITH_OPENMP
      tid = omp_get_thread_num();
    #endif
      memset(chem_req_thread[tid], 0, sizeof(chem_req_thread[tid]));
      memset(chem_remin_thread[tid], 0, sizeof(chem_remin_thread[tid]));
      memset(biomass_thread[tid], 0, sizeof(biomass_thread[tid]));
      memset(agentsInLayer_thread[tid], 0, sizeof(agentsInLayer_thread[tid]));
      memset(agentsInMLD_thread[tid], 0, sizeof(agentsInMLD_thread[tid]));
    #ifdef WITH_OPENMP
    }
    #endif
  }

  static void reduceDiatomsEnv() {
    int t, i, j;
    int threads = omp_get_max_threads();
    for(t = 0; t < threads; t++) {
      for(j = 0; j < 3; j++) {
        for(i = 0; i < 500; i++) {
          chem_req[i][j] += chem_req_thread[t][i][j];
          chem_remin[i][j] += chem_remin_thread[t][i][j];
        }
      }
      for(j = 0; j < 2; j++) {
        biomass[j] += biomass_thread[t][j];
        physics->agentsInMLD[j] += agentsInMLD_thread[t][j];
        for(i = 0; i < 500; i++) {
          physics->agentsInLayer[j][i] += agentsInLayer_thread[t][j][i];
        }
      }
    }
  }
#endif

static void updateAgents() {
  int agent;
  #ifdef WITH_OPENMP
  #pragma omp parallel
  {
  #pragma omp for schedule(runtime) nowait
  #endif
  for (agent = 0; agent < agentCount[_STAGE_Living];  agent++) {
    updateLivingDiatom(agents[_STAGE_Living][agent]);
  }

  #ifdef WITH_OPENMP
    #pragma omp for schedule(runtime) nowait
  #endif
  for (agent = 0; agent < agentCount[_STAGE_Dead]; agent++) {
    updateDeadDiatom(agents[_STAGE_Dead][agent]);
  }
  #ifdef WITH_OPENMP
  }
  #endif
  updateAgentArrays();

  #ifdef WITH_OPENMP
    reduceDiatomsEnv();
    clearThreadsEnv();
  #endif
}

int main(int argc, char **argv) {
  signal(SIGABRT, handler);
  signal(SIGFPE,  handler);
  signal(SIGILL,  handler);
  signal(SIGINT,  handler);
  signal(SIGSEGV, handler);
  signal(SIGTERM, handler);

  # ifdef _OPENMP
  printf("Compiled by an OpenMP-compliant implementation.\n");
  # endif

  #ifdef WITH_OPENMP
  printf("OPENMP requested at compile time.\n");
  #endif

  time_t t = time(NULL);
  printf("LERM Diatom simulation started: %s\n", asctime(gmtime(&t)));

  // Save output of the simulation in the filename passed as an argument.
  struct timeval curTime;
  char outputDir[1000];

  strncpy(outputDir, "../results/output_", 19);

  #ifdef WITH_OPENMP
  strncat(outputDir, "OMP_", 4);
  int maxThreads = omp_get_max_threads();
  char numThreads[3];
  snprintf(numThreads, 3, "%d", maxThreads);
  strncat(outputDir, numThreads, 3);
  strncat(outputDir, "_", 1);
  #endif

  AGENT_LIVING_INIT = 4000;
  AGENT_DEAD_INIT = 0;
  if(argc >= 2) {
    AGENT_LIVING_INIT = atoi(argv[1]);
  }

  // Add special name if requested
  if(argc >= 3) {
    strncat(outputDir, argv[2], strlen(argv[2]));
    strncat(outputDir, "_", 1);
  }

  char timestamp[18];
  gettimeofday(&curTime, NULL);
  snprintf(timestamp, 18, "%lld.txt", (long long) curTime.tv_sec);
  strncat(outputDir, timestamp, 18);

  char physicsPath[] = "data/physics_uncompressed.bin";
  char chemistryPath[] = "data/chemistry_uncompressed.bin";

  chemistryFile = fopen(chemistryPath, "rb");
  if (chemistryFile == NULL) {
    printf ("Error reading: %s\n", chemistryPath);
    exit (1);
  }
  physicsFile = fopen(physicsPath, "rb");
  if (physicsFile == NULL) {
    printf ("Error reading: %s\n", physicsPath);
    exit (1);
  }
  outputFile = fopen(outputDir, "w");
  if (outputFile == NULL) {
    printf ("Error writing: %s\n", outputDir);
    exit (1);
  }
  fprintf(outputFile, "LERM Diatom simulation started: %s\n", asctime(gmtime(&t)) );

  #ifdef WITH_OPENMP
    clearThreadsEnv();
  #endif

  constData = (struct constUpdate*) malloc(sizeof(struct constUpdate));
  physics = (struct physics_data*) malloc(sizeof(struct physics_data));

  physics->noPhysicsLayers = 0;
  biomass[_STAGE_Living] = 0;
  biomass[_STAGE_Dead] = 0;
  stepInHours = 0.5;
  timestep = 0;

  deadMerge = 0;
  // cython code init
  Py_SetProgramName(argv[0]);  /* optional but recommended */
  Py_Initialize();
  PySys_SetArgv(argc, argv); // must call this to get sys.argv and relative imports
  initlibdiatom();

  initialisePlankton();
  initialiseChemistry();

  double envTotal = 0;
  double updateTotal = 0;
  double PMTotal = 0;

  printf ("init: \tL: %#6g\tD: %#6g\tagents: %d\n", biomass[_STAGE_Living], biomass[_STAGE_Dead], agentCount[_STAGE_Living] + agentCount[_STAGE_Dead]);

  struct timespec loopStart, loopEnd, inputStart, inputEnd, updateStart, updateEnd, PMStart, PMEnd;

  clock_gettime(CLOCK_MONOTONIC, &loopStart);
  for (timestep = 0; timestep < 35040; timestep++) {
    clock_gettime(CLOCK_MONOTONIC, &inputStart);
    /* Read environment */
    readPhysics();
    mixChemistry();
    clock_gettime(CLOCK_MONOTONIC, &inputEnd);
    envTotal += (inputEnd.tv_sec - inputStart.tv_sec);
    envTotal += (inputEnd.tv_nsec - inputStart.tv_nsec) / 1000000000.0;

    biomass[_STAGE_Living] = 0.0;
    biomass[_STAGE_Dead] = 0.0;

    /* Update */
    clock_gettime(CLOCK_MONOTONIC, &updateStart);
    updateAgents();
    clock_gettime(CLOCK_MONOTONIC, &updateEnd);
    updateTotal += (updateEnd.tv_sec - updateStart.tv_sec);
    updateTotal += (updateEnd.tv_nsec - updateStart.tv_nsec) / 1000000000.0;

    updateChemistry();

    /* PM */
    clock_gettime(CLOCK_MONOTONIC, &PMStart);
    particleManagement();
    clock_gettime(CLOCK_MONOTONIC, &PMEnd);
    PMTotal += (PMEnd.tv_sec - PMStart.tv_sec);
    PMTotal += (PMEnd.tv_nsec - PMStart.tv_nsec) / 1000000000.0;

    /* Logging */
    if (timestep % 48 == 0) {
      int agentTotal = agentCount[_STAGE_Living] + agentCount[_STAGE_Dead];
      // long int wallclock = clock();
      // long int delta_t =  (wallclock - last_t) *1000 / CLOCKS_PER_SEC;
      // last_t = wallclock;
      printf ("%d\tL: %#6g\tD: %#6g\tagents: %d\n", timestep, biomass[_STAGE_Living], biomass[_STAGE_Dead], agentTotal);
      fprintf (outputFile, "%d\tL:%#6g\tD:%#6g\tag %d\tagL %d\tagD %d\t\t\n", timestep, biomass[_STAGE_Living], biomass[_STAGE_Dead], agentCount[_STAGE_Living] + agentCount[_STAGE_Dead], agentCount[_STAGE_Living], agentCount[_STAGE_Dead]);
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &loopEnd);

  Py_Finalize();
  fclose(physicsFile);
  fclose(chemistryFile);

  double LoopElapsed = (loopEnd.tv_sec - loopStart.tv_sec);
  LoopElapsed += (loopEnd.tv_nsec - loopStart.tv_nsec) / 1000000000.0;

  printf("Loop took %f milliseconds\n", LoopElapsed * 1000);
  printf("Update: %fms\tPM: %fms\tEnv %fms\n", updateTotal * 1000, PMTotal * 1000, envTotal * 1000);

  fprintf(outputFile, "Loop took %f milliseconds\n", LoopElapsed * 1000);
  fprintf(outputFile, "Update: %fms\tPM: %fms\tEnv %fms\n", updateTotal * 1000, PMTotal * 1000, envTotal * 1000);

  t = time(NULL);
  printf("LERM Diatom simulation finished: %s\n", asctime(gmtime(&t)));
  fprintf(outputFile, "LERM Diatom simulation finished: %s\n", asctime(gmtime(&t)));

  fclose(outputFile);
  free(constData);
  free(physics);
  return (0);
}
