from distutils.core import run_setup

dist = run_setup("setup.py", script_args=["build_ext", "--inplace"], stop_after="config")

dist.parse_command_line()

dist.run_commands()
